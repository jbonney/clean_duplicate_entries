# Clean Duplicate Email Entries

## Summary

The script reads a CSV file containing email addresses and cleans it from duplicate addresses. In addition, if multiple emails are present in the email column, they will be separated and written in new lines in the output file (see current limitations about the format below). Finally, the email addresses format will be checked and the domain name will be analyzed to identify if there is a valid MX server associated to it.

Here under is an example of an input file and the output file produced by the script.

Input file:

    firstname;lastname;email
    John;Doe;john@test
    Jane;Doe;jane@domain_exists.com, jane@domain_does_not_exist.com

Output file:

    firstname;lastname;email;validity
    John;Doe;john@test,invalid format
    Jane;Doe;jane@domain_exists.com
    Jane;Doe;jane@domain_does_not_exist.com,invalid domain

## Installation

1. Install the necessary gem: `bundle install`
2. Ensure that your ruby path is correctly defined (see first line in the script)
3. See the current limitation list and adapt the source to your needs
4. Make the script executable: `chmod +x clean_duplicate_entries.rb`

## Execution

Simply run the executable file and specify the input and output files. A temporary folder will also be created under `/tmp` so it assumes that you have the necessary rights for this folder as well.

    ./clean_duplicate_entries.rb /path/to/input.csv /path/to/output.csv

An additional option is available in order to filter out invalid email addresses and domain names. In order to do that, simply add one last parameter to the command line:

    ./clean_duplicate_entries.rb /path/to/input.csv /path/to/output.csv filter

If this option is not passed along, the final file will simply add a new field containing `invalid format` or `invalid domain` at the end of the lines containing invalid email addresses.

## Current limitations

1. The input CSV file should have header columns, one of which should be named "email".
2. The input format should not matter much. The header of the file will however be converted to ascii.
3. Current separator used in the file is set to ";".
4. Current separator used in the email column is set to ",".
4. Internet connection is required in order to test the validity of the email servers.

## License

Licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.html).