#!/usr/bin/env ruby

# Identify entries with multiple emails on one line.
# Create one line per email.
# Identify duplicate email entries and remove them.
# Verify the format of the email address and whether a MX server exists.
# @author: Jimmy Bonney (jb@bnyconsulting.com)
# @created_at: 2012-06-18
# @sponsor: BNY Consulting
# @version: 1.1

# Dependencies
require 'rubygems'
require 'fileutils'
require 'date'
require 'thread'
require 'csv'
require 'benchmark'
require 'valid_email'

# Script directory
@@path = File.expand_path(File.dirname(__FILE__))

# Check if there is any argument
if !ARGV || ARGV.count < 2
  puts ""
  puts "usage: ./clean_duplicate_entries path/to/input.csv path/to/output.csv [filter]"
  puts ""
  exit
end

# Define the class containing contact information
class Contact
  include ActiveModel::Validations
  attr_accessor :email

  validates :email, :email => true
end

# Class containing the email verification logic
class EmailChecker
  # List of all domains (acts as cache) and mutex to prevent concurrent writing to the list
  attr_accessor :domain_list, :mutex_domain, :generic_domains_list

  def initialize(args = {})
    self.domain_list = {}
    self.mutex_domain = Mutex.new
    self.generic_domains_list = [
      "gmail.com",
      "wanadoo.fr",
      "orange.fr",
      "tiscali.fr",
      "yahoo.fr",
      "yahoo.com",
      "hotmail.fr",
      "hotmail.com",
      "free.fr",
      "club-internet.fr",
      "outlook.fr",
      "outlook.com",
      "live.fr",
      "live.com",
      "libertysurf.fr",
      "voila.fr",
      "sfr.fr",
      "aliceadsl.fr",
      "cegetel.net",
      "aol.com",
      "laposte.net",
      "bbox.fr",
      "gouv.fr",
      "skynet.be",
      " "
    ]
  end

  # Run the verification
  def verify(email)
    begin
      messages = []
      format = valid_format?(email)
      messages << "Invalid format" unless format == true
      if format
        generic_domain = generic_domain?(email)
        messages << "Generic domain" if generic_domain == true
          if !generic_domain
          domain = valid_domain?(email)
          messages << "Invalid domain" unless domain == true
        end
      end
      return messages
    rescue exception
      p "Error during email verification for #{email}: #{exception.inspect}"
    end
  end

  protected

  # Validates the format of the email
  def valid_format?(email)
    contact = Contact.new
    contact.email = email
    return contact.valid?
  end

  # # Ignore gmail, orange and other generic domains
  def generic_domain?(email)
    # Takes the last part of the email address
    domain = email.match(/\@(.+)/)[1]
    # Ignore the address if it is part of the generic domain list
    return @generic_domains_list.include?(domain)
  end

  # Validates the domain
  def valid_domain?(email)
    # Takes the last part of the email address
    domain = email.match(/\@(.+)/)[1]
    # Only runs if the domain was not checked previously
    unless self.domain_list[domain]
      # print "Validating domain #{domain} ... "
      mx = []
      begin
        Resolv::DNS.open do |dns|
          mx.concat dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
        end
        valid_mail_server = mx.size > 0
        self.mutex_domain.synchronize do
          self.domain_list[domain] = valid_mail_server
        end
      rescue Encoding::CompatibilityError => e
        self.mutex_domain.synchronize do
          self.domain_list[domain] = false
        end
      end
      # print "#{valid_mail_server ? "OK" : "Incorrect"}\n"
    end
    return self.domain_list[domain]
  end
end


#
# Clean duplicate entries from a CSV file
#
# @author Jimmy
class CleanDuplicatesEntries

  attr_accessor :input_file, :output_file, :input_headers, :input_headers_string, :unique_emails, :mail_checker, :mutex_email, :processing_name, :tmp_folder, :filter_invalid

  def initialize(args)
    # Create a directory to store the input file and the partial input files
    self.tmp_folder = "/tmp/clean_duplicate_entries"
    unless File.directory?(self.tmp_folder)
      Dir.mkdir(self.tmp_folder)
    end

    # Copy the input file to a temporary file.
    # This allows to mess with the headers by removing any special characters
    time = Time.now.to_i
    self.processing_name = "input_#{time}"
    FileUtils.cp args[0], "#{self.tmp_folder}/#{self.processing_name}.csv"

    # puts "Initializers args: #{args}"
    self.input_file = "#{self.tmp_folder}/#{self.processing_name}.csv"
    self.output_file = args[1]

    # Check if the filter_invalid argument is passed
    self.filter_invalid = (args[2] == "filter") ?  true : false

    # Replace the first line of the file by ascii characters so that they can be converted by CSV.read
    self.input_headers_string = File.open(self.input_file, &:readline).encode('ascii', :invalid => :replace, :undef => :replace)
    File.open(self.input_file, 'r+'){ |f| f.write self.input_headers_string}

    # Get the header of the file -> will allow to create temporary files for parallel processing
    self.input_headers = CSV.read(self.input_file, {col_sep: ";", headers: true, return_headers: true, header_converters: :symbol}).headers

    # Initiate the list of unique email addresses
    self.unique_emails = {}

    # Define the mail checker object
    self.mail_checker = EmailChecker.new

    # Set up the mutex for the unique email list
    self.mutex_email = Mutex.new
  end

  #
  # Create chunk files based on the input file.
  # The header line of the input file is copied over to each chunk.
  # Chunks will be named with the following suffixes: _0.csv, _1.csv, etc.
  #
  # @param  f_in [String] Path to the input file
  # @param  out_pref [String] Prefix to be used for the chunk file names
  # @param  chunksize = 50000 [integer] The size of each chunk
  #
  # @return [integer] the number of chunks that have been created
  def chunker f_in, out_pref, chunksize = 50000
    outfilenum = 0
    File.open(f_in,"r") do |fh_in|
      until fh_in.eof?
        line_count = 0
        File.open("#{self.tmp_folder}/#{out_pref}_#{outfilenum}.csv","w") do |fh_out|
          line = ""
          # First line of the file should contain the headers
          fh_out << self.input_headers_string unless outfilenum == 0
          while line_count <= chunksize && !fh_in.eof?
            line = fh_in.readline
            fh_out << line
            line_count += 1
          end
        end
        outfilenum += 1
      end
    end
    return outfilenum + 1
  end

  #
  # Filter unique emails in the file
  # @param  chunks = 50 [integer] The number of chunks to the created
  #
  # @return [type] [description]
  def process_file(chunks = 50)

    # Number of lines
    line_count = CSV.read(self.input_file, {col_sep: ";", headers: true, return_headers: false}).size

    # Create the chunks
    file_count = chunker self.input_file, self.processing_name, line_count / chunks

    # Define the invalid domain counter
    invalid_email_counter = 0
    # Define the number of line read
    line_read = 0
    # Define the number of email address read
    email_read = 0

    # Set mutex on the different variables that will be written by the different threads
    mutex_line = Mutex.new
    mutex_email_counter = Mutex.new
    mutex_invalid_counter = Mutex.new
    mutex_output = Mutex.new

    # Open the output file
    CSV.open(self.output_file, "a", {col_sep: ";", headers: false}) do |csv|
      # Set the header in the file
      csv << self.input_headers
      # Prepare the threads
      threads = []
      file_count.times do |count|
        threads[count] = Thread.new {
          # Ensure that the chunk file exists
          if File.exists?("#{self.tmp_folder}/#{self.processing_name}_#{count}.csv")
            # Read the input file - silently swallow the headers
            CSV.foreach("#{self.tmp_folder}/#{self.processing_name}_#{count}.csv", {col_sep: ";", headers: true, return_headers: false, header_converters: :symbol}) do |row|
              # Progress information
              mutex_line.synchronize do
                # Record the number of line read
                line_read += 1
                # Progress indicator
                if line_read % 100 == 0
                  puts "#{line_read} lines processed from input file (#{invalid_email_counter} invalid or generic domains so far)"
                end
              end
              # Verify if there are multiple emails in the line
              emails = row[:email].split "," unless row[:email].nil?
              if emails && emails.size > 1
                # Create a new line for each email
                emails.each do |email|
                  mutex_email_counter.synchronize do
                    email_read += 1
                  end
                  if unique_address?(email.strip)
                    # Write the new entry line to the output file
                    new_entry = create_entry(row, email)
                    mutex_output.synchronize do
                      csv << new_entry if new_entry
                    end
                    mutex_invalid_counter.synchronize do
                      if filter_invalid
                        invalid_email_counter += 1 if new_entry.nil?
                      else
                        invalid_email_counter += 1 if new_entry.size > self.input_headers.size
                      end
                    end
                  end
                end
              elsif emails
                email = row[:email].strip
                mutex_email_counter.synchronize do
                  email_read += 1
                end
                if unique_address?(email)
                  # Write the new entry line to the output file
                  new_entry = create_entry(row, email)
                  mutex_output.synchronize do
                    csv << new_entry if new_entry
                  end
                  mutex_invalid_counter.synchronize do
                    if filter_invalid
                      invalid_email_counter += 1 if new_entry.nil?
                    else
                      invalid_email_counter += 1 if new_entry.size > self.input_headers.size
                    end
                  end
                end
              end
            end
          end
        }
      end
      # Wait for the different threads to finish their jobs
      threads.each {|t| t.join }
    end
    puts ""
    puts "Number of lines processed: #{line_read}"
    puts "Number of email addresses processed: #{email_read}"
    puts "Number of unique email entries: #{self.unique_emails.size}"
    puts "Number of unique mail domains: #{self.mail_checker.domain_list.size}"
    puts "Number of invalid or generic email addresses: #{invalid_email_counter}"
  end

  #
  # Verify if the email address already exists in the cache
  # @param  email_address [String] the email address
  #
  # @return [Boolean] true if the email address doesn't exist already; false otherwise
  def unique_address?(email_address)
    # Ensure uniqueness in a mutex so that there is no race condition between the different threads
    self.mutex_email.synchronize do
      address_exists = self.unique_emails[email_address]
      if address_exists.nil?
        self.unique_emails[email_address] = 1
      else
        self.unique_emails[email_address] += 1
      end
      # address_exists.nil? returns true if the email address is not yet in the unique_emails hash which means that it is unique
      return address_exists.nil? ? true : false
    end
  end

  #
  # Create the new line to be added in the CSV file
  # @param  row [Array] the original data
  # @param  email [String] the email address
  #
  # @return [Array] the new line to be written
  # @return [nil] if filter_invalid is set, return nil instead of an array if email is deemed invalid
  def create_entry(row, email)
    new_line = []
    self.input_headers.each do |field|
      if field == :email
        new_line << email.strip
      else
        new_line << row[field]
      end
    end
    # Check domain validity
    verification_messages = self.mail_checker.verify(email.strip)
    if verification_messages.size > 0
      # If filter_invalid is set, do not create a line and return nil instead
      if self.filter_invalid
        new_line = nil
      else
        new_line << verification_messages.join(". ")
      end
    end
    return new_line
  end

end

file_processor = CleanDuplicatesEntries.new(ARGV)
time = Benchmark.measure do
  file_processor.process_file
end
# Delete temporary files
FileUtils.rm_rf(file_processor.tmp_folder)
puts "Output generated in #{time}"
