#!/bin/bash

# http://www.linuxquestions.org/questions/general-10/bash-script-using-getopt-to-parse-optional-arguments-877176/
while getopts f:l: opt
do
  case $opt in
    f) filename="${OPTARG}"
       if [ "$filename" == "" ]
       then
          print_usage $0
       fi ;;
    l) line="$OPTARG";;
    ?) print_usage $0
       exit 1;;
  esac
done

if [[ -z $filename  ]] ; then
  echo "A filename must be supplied (with the "-f" flag)."
  exit 1
elif [[ ! -e $filename ]]; then
  echo "The filename provided does not exist."
  exit 1
elif [[ ! -r $filename ]]; then
  echo "The filename provided is not readable.  Check your permissions."
  exit 1
fi

if [[ -z $line  ]] ; then
  echo "A number of line for the new files must be supplied (with the "-l" flag)."
  exit 1
fi

# http://stackoverflow.com/questions/1411713/how-to-split-a-file-and-keep-the-first-line-in-each-of-the-pieces
# tail -n +2 $filename | split -l $line - split_
# for file in split_*
# do
#   head -n 1 $filename > tmp_file
#   cat $file >> tmp_file
#   mv -f tmp_file $file.csv
# done

# http://i.nt.ro/splitting-a-csv-file-using-bash-in-linux/
# create a directory to store the output:
mkdir split

# create a temporary file containing the header without the content:
head -n 1 $filename > header.csv

# create a temporary file containing the content without the header:
tail -n +2 $filename > content.csv

# split the content file into multiple files of $line lines each:
split -l $line content.csv split/data_

# loop through the new split files, adding the header# and a '.csv' extension:
for f in split/*; do cat header.csv $f > $f.csv; rm $f; done;

# remove the temporary files:
rm header.csv
rm content.csv